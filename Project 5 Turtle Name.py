import turtle
turtle.color('tomato')
turtle.pensize(5)
turtle.shape('turtle')

turtle.penup()
turtle.backward(320)
turtle.pendown()
#S
turtle.forward(100)
turtle.backward(100)
turtle.right(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(100)

turtle.penup()
turtle.backward(120)
turtle.right(90)
turtle.pendown()
#H
turtle.color('dodgerblue')
turtle.forward(200)
turtle.backward(100)
turtle.right(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(100)
turtle.backward(200)

turtle.penup()
turtle.left(90)
turtle.forward(20)
turtle.pendown()

#U
turtle.color('lime')
turtle.right(90)
turtle.forward(200)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(200)

turtle.right(90)
turtle.penup()
turtle.forward(20)
turtle.pendown()

#V
turtle.color('seagreen')
turtle.right(80)
turtle.forward(210)
turtle.left(150)
turtle.forward(210)


turtle.penup()
turtle.right(70)
turtle.forward(20)
turtle.pendown()

#O
turtle.color('blue')
turtle.forward(100)
turtle.right(90)
turtle.forward(200)
turtle.right(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(200)

#text
turtle.penup()
turtle.right(180)
turtle.forward(200+70)
turtle.right(90)
turtle.forward(320)
turtle.pendown()
turtle.shape('arrow')
turtle.pensize(1)
turtle.color('red')
turtle.write('That\'s my name...',font=('consolas',25,'bold'))
turtle.penup()
turtle.exitonclick()

