def displayMenu():
	print('Text Speaker Converter:')
	print('='*23)
	print('Menu:')
	print('		c = Convert sentence')
	print('		p = Print the dictionary')
	print('		a = Add a word')
	print('		d = Delete a word')
	print('		q = Quit')

def convertSentence():
	sentence = input('Enter a sentence:').lower()
	wordsToTranslate = sentence.split()
	tranlatedSentence = ''
	for word in wordsToTranslate:
		if word in textSpeakDictionary:
			tranlatedSentence +=textSpeakDictionary[word] + ' '
		else:
			tranlatedSentence += word + ' '
	print('==>')
	print(tranlatedSentence)

def textAdd():
	textToAdd = input('Enter the text-speak you want to add in the dictionary:')
	meaning = input('What does it mean?')
	textSpeakDictionary[textToAdd] = meaning

def textDelete():
	textToDelete = input('Enter the text-speak you want to delete from the dictionary:')
	del textSpeakDictionary[textToDelete]
	

textSpeakDictionary = {'lol':'laugh out loud',
					   'idk' : 'i don\'t know',
					   'jk' : 'just kidding',
					   '143' : 'i love u',
					   'pia' : 'pic for attention',
					   }
displayMenu()

running = True
while running == True:   
	menuChoice = input('Enter a choice from the menu bar>_').lower()
	if menuChoice == 'a':
		textAdd()
	elif menuChoice == 'c':
		convertSentence()
	elif menuChoice == 'd':
		textDelete()
	elif menuChoice == 'p':
		print(textSpeakDictionary)
	elif menuChoice =='q':
		running = False
	else:
		print('Invalid menu choice.')
